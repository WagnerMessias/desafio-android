package br.eti.wagnermessias.javapop.services;

import java.util.List;

import br.eti.wagnermessias.javapop.entities.PullRequest;
import br.eti.wagnermessias.javapop.entities.RepositoriesResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ServicesContract {

    interface Repositories {
        void getRepositories(int page);
    }

    interface PullRequests {
        void getPullRequests(String creator, String repository);
    }

    interface GithubAPI {
        @GET("/search/repositories")
        Call<RepositoriesResponse> getRepositories(@Query("q") String q, @Query("sort") String sort, @Query("page") int page);

        @GET("/repos/{creator}/{repository}/pulls")
        Call<List<PullRequest>> getPullrequests(@Path("creator") String creator, @Path("repository") String repository);
    }
}
