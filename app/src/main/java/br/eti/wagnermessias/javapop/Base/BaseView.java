package br.eti.wagnermessias.javapop.Base;

public interface BaseView<T> {
    void setPresenter(T presenter);

}
