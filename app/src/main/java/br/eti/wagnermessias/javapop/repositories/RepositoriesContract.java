package br.eti.wagnermessias.javapop.repositories;

import android.content.Context;

import java.io.File;
import java.util.List;

import br.eti.wagnermessias.javapop.Base.BasePresenter;
import br.eti.wagnermessias.javapop.Base.BaseView;
import br.eti.wagnermessias.javapop.entities.Repository;

public interface RepositoriesContract {

    interface View extends BaseView<Presenter> {

        List<Repository> getRepositories();

        void initRecyclerView(List<Repository> repositories);

        void updateRecyclerView(List<Repository> repositories);

        void showError(String msgErro);

        void showLoadingRecyclerView(boolean showLoadingUI);

        void toDecreasePage();

        Context getContexto();
    }

    interface Presenter extends BasePresenter {
        void addRepositories(List<Repository> repositories);

        void loadRepositories(int page);

        void showErroDisplay(String msgErro);

        void showErroDisplay(String msgErro, boolean toDecreasePage);

        File getCacheDir();

        boolean isNetworkAvailable();
    }
}
