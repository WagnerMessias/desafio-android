package br.eti.wagnermessias.javapop.pullrequests;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.eti.wagnermessias.javapop.R;
import br.eti.wagnermessias.javapop.entities.PullRequest;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PullRequestsActivity extends AppCompatActivity implements PullRequestsContract.View, PullsRequestsAdapter.ItemClickListener {

    @BindView(R.id.rv_pullrequests)
    RecyclerView mRecyclerView;

    @BindView(R.id.pullrequests_progress_bar)
    ProgressBar pullrequests_progressBar;

    @BindView(R.id.pullrequest_tv_not_pullsrequests)
    TextView pullrequest_tv_not_pullsrequests;

    @BindView(R.id.qtd_open)
    TextView tv_qtd_open;

    @BindView(R.id.qtd_closed)
    TextView tv_qtd_closed;

    private Context mContext;
    private LinearLayoutManager layoutManager;

    public List<PullRequest> getPullsRequests() {
        return pullsRequests;
    }

    private List<PullRequest> pullsRequests = new ArrayList<>();
    private PullsRequestsAdapter adapter;
    private PullRequestPresenter presenter;
    private int page = 1;
    private String creator;

        private String repository;
    private int qtdOpen;
    private int qtdClosed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pullrequests);
        ButterKnife.bind(this);

        mContext = this;
        init();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void init() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ActionBar ab = getSupportActionBar();

        presenter = new PullRequestPresenter(this);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            creator = b.getString("key_creator");
            repository = b.getString("key_repository");
            presenter.loadPullsRequests(creator, repository);
            ab.setTitle(repository);
        } else {
            pullrequest_tv_not_pullsrequests.setVisibility(View.VISIBLE);
            ab.setTitle("Github JavaPop PullsRequestes");
        }

    }

    public void initRecyclerView(List<PullRequest> pullsRequests) {
        pullrequest_tv_not_pullsrequests.setVisibility(View.GONE);
        this.pullsRequests = pullsRequests;
        mRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        adapter = new PullsRequestsAdapter(this, this.pullsRequests);
        adapter.setClickListener(this);
        mRecyclerView.setAdapter(adapter);
        showLoadingRecyclerView(false);

        countStates();
    }

    private void countStates() {
        qtdClosed = 0;
        qtdOpen = 0;
        for (PullRequest pullRequest : pullsRequests) {
            if (pullRequest.getState().equals("open")) {
                ++qtdOpen;
            } else {
                ++qtdClosed;
            }
        }
        tv_qtd_open.setText(qtdOpen + " abertos |");
        tv_qtd_closed.setText(qtdClosed + " fechados");
    }

    @Override
    public void showError(String msgErro) {
        showLoadingRecyclerView(false);
        Toast.makeText(this, msgErro, Toast.LENGTH_SHORT).show();
        if (pullsRequests == null || pullsRequests.size() < 1) {
            pullrequest_tv_not_pullsrequests.setVisibility(View.VISIBLE);
        } else {
            pullrequest_tv_not_pullsrequests.setVisibility(View.GONE);
        }
    }

    @Override
    public void setPresenter(PullRequestsContract.Presenter presenter) {
        presenter = presenter;
    }


    @Override
    public Context getContexto() {
        return this.mContext;
    }

    @Override
    public void showLoadingRecyclerView(boolean showLoadingUI) {

        if (showLoadingUI) {
            pullrequests_progressBar.setVisibility(View.VISIBLE);
        } else {
            pullrequests_progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        String url = pullsRequests.get(position).getHtmlUrl();
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public void setRepository(String repository) {
        this.repository = repository;
    }
}
