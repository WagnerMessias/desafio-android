package br.eti.wagnermessias.javapop.services;

import java.util.List;

import br.eti.wagnermessias.javapop.entities.PullRequest;
import br.eti.wagnermessias.javapop.pullrequests.PullRequestsContract;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PullRequestsService implements ServicesContract.PullRequests {

    public final PullRequestsContract.Presenter presenter;
    public final static String URL_BASE = "https://api.github.com";
    public final static int CACHE_SIZE = 10 * 1024 * 1024; // 10 MB
    private Retrofit mRetrofit;

    public PullRequestsService(final PullRequestsContract.Presenter presenter) {

        this.presenter = presenter;

        Cache cache = new Cache(presenter.getCacheDir(), CACHE_SIZE);// 10 MB

        OkHttpClient client = new OkHttpClient
                .Builder()
                .cache(cache)
                .build();

        this.mRetrofit = new Retrofit.Builder()
                .baseUrl(URL_BASE)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Override
    public void getPullRequests(String creator, String repository) {
        ServicesContract.GithubAPI service = mRetrofit.create(ServicesContract.GithubAPI.class);

        Call<List<PullRequest>> call = service.getPullrequests(creator, repository);

        call.enqueue(new Callback<List<PullRequest>>() {
            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                if (response.isSuccessful()) {
                    List<PullRequest> pullsTequests = response.body();
                    presenter.addPullsRequests(pullsTequests);
                }
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {
                presenter.showErroDisplay("Servidor indisponível no momento!");
            }
        });

    }
}
