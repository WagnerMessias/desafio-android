package br.eti.wagnermessias.javapop.services;

import android.content.Context;

import java.util.List;

import br.eti.wagnermessias.javapop.entities.RepositoriesResponse;
import br.eti.wagnermessias.javapop.entities.Repository;
import br.eti.wagnermessias.javapop.repositories.RepositoriesContract;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RepositoriesService implements ServicesContract.Repositories {

    public final String QUERY = "language:Java";
    public final String SORT = "stars";
    public final RepositoriesContract.Presenter presenter;
    public final static String URL_BASE = "https://api.github.com";
    public final static int CACHE_SIZE = 10 * 1024 * 1024; // 10 MB
    private Retrofit mRetrofit;
    private Context contextView;

    public RepositoriesService(final RepositoriesContract.Presenter presenter) {

        this.presenter = presenter;

        Cache cache = new Cache(presenter.getCacheDir(), CACHE_SIZE);// 10 MB

        OkHttpClient client = new OkHttpClient
                .Builder()
                .cache(cache)
                .build();

        this.mRetrofit = new Retrofit.Builder()
                .baseUrl(URL_BASE)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Override
    public void getRepositories(int page) {
        ServicesContract.GithubAPI service = mRetrofit.create(ServicesContract.GithubAPI.class);

        Call<RepositoriesResponse> call = service.getRepositories(QUERY, SORT, page);

        call.enqueue(new Callback<RepositoriesResponse>() {
            @Override
            public void onResponse(Call<RepositoriesResponse> call, Response<RepositoriesResponse> response) {
                if (response.isSuccessful()) {
                    response.body();
                    List<Repository> repositories = response.body().getItems();
                    presenter.addRepositories(repositories);
                }
            }

            @Override
            public void onFailure(Call<RepositoriesResponse> call, Throwable t) {
                presenter.showErroDisplay("Servidor indisponível no momento!");
            }
        });
    }
}
