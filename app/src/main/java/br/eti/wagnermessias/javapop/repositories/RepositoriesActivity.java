package br.eti.wagnermessias.javapop.repositories;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.pwittchen.infinitescroll.library.InfiniteScrollListener;

import java.util.ArrayList;
import java.util.List;

import br.eti.wagnermessias.javapop.R;
import br.eti.wagnermessias.javapop.entities.Repository;
import br.eti.wagnermessias.javapop.pullrequests.PullRequestsActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RepositoriesActivity extends AppCompatActivity implements RepositoriesContract.View, RepositoriesAdapter.ItemClickListener {

    @BindView(R.id.rv_repositories)
    RecyclerView mRecyclerView;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.btn_tray_again)
    Button btn_tray_again;

    private Context mContext;
    private LinearLayoutManager layoutManager;
    private List<Repository> repositories = new ArrayList<>();
    private RepositoriesAdapter adapter;
    private RepositoriesPresenter presenter;
    public int page = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repositories);
        ButterKnife.bind(this);
        mContext = this;
        init();
    }

    public void init(){
        ActionBar ab = getSupportActionBar();
        ab.setTitle("Github JavaPop");
        presenter = new RepositoriesPresenter(this);
        presenter.start();
    }

    public void initRecyclerView(List<Repository> repositories){
        btn_tray_again.setVisibility(View.GONE);
        this.repositories = repositories;
        mRecyclerView.setHasFixedSize(true);
        layoutManager  =   new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        adapter = new RepositoriesAdapter(this, this.repositories);
        adapter.setClickListener(this);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.addOnScrollListener(createInfiniteScrollListener());
        showLoadingRecyclerView(false);
    }

    public void updateRecyclerView(List<Repository> repositories) {
        btn_tray_again.setVisibility(View.GONE);
        showLoadingRecyclerView(false);
        this.repositories = repositories;
        adapter.notifyDataSetChanged();
    }

    @NonNull
    private InfiniteScrollListener createInfiniteScrollListener() {
        return new InfiniteScrollListener(4,layoutManager) {
            @Override public void onScrolledToEnd(final int firstVisibleItemPosition) {
                ++page;
                presenter.loadRepositories(page);

            }
        };
    }

    @Override
    public void showError(String msgErro){
        showLoadingRecyclerView(false);
        Toast.makeText(this, msgErro, Toast.LENGTH_SHORT).show();

        if (repositories == null || repositories.size() < 1) {
            btn_tray_again.setVisibility(View.VISIBLE);
        } else {
            btn_tray_again.setVisibility(View.GONE);
        }

    }

    @Override
    public void setPresenter(RepositoriesContract.Presenter presenter) {
        presenter = presenter;
    }

    @Override
    public List<Repository> getRepositories(){
        return repositories;
    }

    @Override
    public Context getContexto() {
        return this.mContext;
    }

    @Override
    public void showLoadingRecyclerView(boolean showLoadingUI) {

        if (showLoadingUI) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void toDecreasePage() {
        if(page > 1){
            --page;
        }
    }

    @OnClick(R.id.btn_tray_again)
    public void onButtonClicked() {
        presenter.loadRepositories(page);
    }

    @Override
    public void onItemClick(View view, int position) {

        String creator = repositories.get(position).getOwner().getLogin();
        String  repository = repositories.get(position).getName();

        Intent intent = new Intent(RepositoriesActivity.this, PullRequestsActivity.class);
        Bundle b = new Bundle();
        b.putString("key_creator", creator);
        b.putString("key_repository", repository);
        intent.putExtras(b);
        startActivity(intent);
    }

}
