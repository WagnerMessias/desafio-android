package br.eti.wagnermessias.javapop.pullrequests;

import android.content.Context;

import java.io.File;
import java.util.List;

import br.eti.wagnermessias.javapop.Base.BasePresenter;
import br.eti.wagnermessias.javapop.Base.BaseView;
import br.eti.wagnermessias.javapop.entities.PullRequest;

public interface PullRequestsContract {

    interface View extends BaseView<Presenter> {

        void initRecyclerView(List<PullRequest> pullsRequests);

        void showError(String msgErro);

        void showLoadingRecyclerView(boolean showLoadingUI);

        Context getContexto();
    }

    interface Presenter extends BasePresenter {

        void addPullsRequests(List<PullRequest> pullsRequests);

        void loadPullsRequests(String creator, String repository);

        void showErroDisplay(String msgErro);

        File getCacheDir();
    }
}
