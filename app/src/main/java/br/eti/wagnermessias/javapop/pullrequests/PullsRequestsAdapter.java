package br.eti.wagnermessias.javapop.pullrequests;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.eti.wagnermessias.javapop.R;
import br.eti.wagnermessias.javapop.entities.PullRequest;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PullsRequestsAdapter extends RecyclerView.Adapter<PullsRequestsAdapter.ViewHolder> {


    public List<PullRequest> pullsRequests;
    private ItemClickListener mClickListener;
    private LayoutInflater mInflater;
    private Context mContexto;


    PullsRequestsAdapter(Context context, List<PullRequest> pullsRequests) {
        this.mInflater = LayoutInflater.from(context);
        this.pullsRequests = pullsRequests;
        this.mContexto = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_pullrequests, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PullRequest pullRequest = pullsRequests.get(position);
        holder.title.setText(pullRequest.getTitle());
        holder.body.setText(pullRequest.getBody());
        holder.username.setText(pullRequest.getUser().getLogin());

        Glide.with(mContexto)
                .load(pullRequest.getUser().getAvatarUrl())
                .placeholder(R.drawable.ic_placeholder_github)
                .centerCrop()
                .into(holder.avatar);

    }

    @Override
    public int getItemCount() {
        return pullsRequests.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tv_title_pullrequest)
        TextView title;

        @BindView(R.id.pullrequest_tv_body)
        TextView body;

        @BindView(R.id.pullrequest_tv_user_name)
        TextView username;

        @BindView(R.id.pullrequest_iv_user)
        ImageView avatar;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    PullRequest getItem(int id) {
        return pullsRequests.get(id);
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
