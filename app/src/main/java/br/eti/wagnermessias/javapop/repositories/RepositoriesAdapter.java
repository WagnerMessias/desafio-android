package br.eti.wagnermessias.javapop.repositories;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.eti.wagnermessias.javapop.R;
import br.eti.wagnermessias.javapop.entities.Repository;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesAdapter.ViewHolder> {


    public List<Repository> repositories;
    private ItemClickListener mClickListener;
    private LayoutInflater mInflater;
    private Context mContexto;


    RepositoriesAdapter(Context context, List<Repository> repositories) {
        this.mInflater = LayoutInflater.from(context);
        this.repositories = repositories;
        this.mContexto = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_repositories, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Repository repository = repositories.get(position);
        holder.name_repository.setText(repository.getName());
        holder.description.setText(repository.getDescription());
        holder.username.setText(repository.getOwner().getLogin());
        holder.username.setText(repository.getOwner().getLogin());
        holder.qtd_fork.setText(repository.getForks().toString());
        holder.qtd_star.setText(repository.getWatchers().toString());

        Glide.with(mContexto)
                .load(repository.getOwner().getAvatarUrl())
                .placeholder(R.drawable.ic_placeholder_github)
                .centerCrop()
                .into(holder.avatar);

    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tv_name_repository)
        TextView name_repository;

        @BindView(R.id.tv_description)
        TextView description;

        @BindView(R.id.tv_user_name)
        TextView username;

        @BindView(R.id.tv_qtd_fork)
        TextView qtd_fork;

        @BindView(R.id.tv_qtd_star)
        TextView qtd_star;

        @BindView(R.id.iv_user)
        ImageView avatar;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    Repository getItem(int id) {
        return repositories.get(id);
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
