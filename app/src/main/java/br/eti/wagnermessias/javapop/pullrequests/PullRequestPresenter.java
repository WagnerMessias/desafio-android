package br.eti.wagnermessias.javapop.pullrequests;

import android.support.annotation.NonNull;

import java.io.File;
import java.util.List;

import br.eti.wagnermessias.javapop.entities.PullRequest;
import br.eti.wagnermessias.javapop.helpers.NetworkHelper;
import br.eti.wagnermessias.javapop.services.PullRequestsService;

public class PullRequestPresenter implements PullRequestsContract.Presenter {

    private PullRequestsContract.View viewPullRequest;
    private PullRequestsService servicesPullRequests;
    private File cacheDir;


    public PullRequestPresenter(@NonNull PullRequestsContract.View view) {
        this.cacheDir = view.getContexto().getCacheDir();
        this.servicesPullRequests = new PullRequestsService(this);
        this.viewPullRequest = view;
    }

    public File getCacheDir() {
        return cacheDir;
    }

    @Override
    public void start() {
    }

    @Override
    public void loadPullsRequests(String creator, String repository) {
        loadRepositories(true, creator, repository);
    }

    private void loadRepositories(final boolean showLoadingUI, String creator, String repository) {

        if (showLoadingUI) {
            viewPullRequest.showLoadingRecyclerView(true);
        }

        if (NetworkHelper.verificaConexao(viewPullRequest.getContexto())) {
            servicesPullRequests.getPullRequests(creator, repository);
        } else {
            showErroDisplay("Sem conexão com Internet");
        }

    }

    @Override
    public void addPullsRequests(List<PullRequest> pullsRequests) {

        if (pullsRequests.size() > 0) {
            viewPullRequest.initRecyclerView(pullsRequests);
        } else {
            viewPullRequest.showError("Nenhum repositório foi encontrado");
        }
    }

    @Override
    public void showErroDisplay(String msgErro) {
        viewPullRequest.showError(msgErro);
    }


}
