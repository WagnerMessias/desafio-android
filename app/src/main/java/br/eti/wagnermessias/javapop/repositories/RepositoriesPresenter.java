package br.eti.wagnermessias.javapop.repositories;

import android.support.annotation.NonNull;

import java.io.File;
import java.util.List;

import br.eti.wagnermessias.javapop.entities.Repository;
import br.eti.wagnermessias.javapop.helpers.NetworkHelper;
import br.eti.wagnermessias.javapop.services.RepositoriesService;

public class RepositoriesPresenter implements RepositoriesContract.Presenter {


    private boolean mFirstLoad = true;

    private RepositoriesContract.View viewRepositories;
    private RepositoriesService servicesRepositories;
    private File cacheDir;


    public RepositoriesPresenter(@NonNull RepositoriesContract.View view) {
        this.cacheDir = view.getContexto().getCacheDir();
        this.servicesRepositories = new RepositoriesService(this);
        this.viewRepositories = view;
    }

    public File getCacheDir() {
        return cacheDir;
    }

    public boolean isNetworkAvailable() {
        return NetworkHelper.verificaConexao(viewRepositories.getContexto());
    }

    @Override
    public void start() {
        loadRepositories(1);
    }

    @Override
    public void loadRepositories(int page) {
        loadRepositories(true, page);
    }

    private void loadRepositories(final boolean showLoadingUI, int page) {

        if (showLoadingUI) {
            viewRepositories.showLoadingRecyclerView(true);
        }

        if (NetworkHelper.verificaConexao(viewRepositories.getContexto())) {
            servicesRepositories.getRepositories(page);
        } else {
            showErroDisplay("Sem conexão com Internet", true);
        }

    }

    @Override
    public void addRepositories(List<Repository> repositoriesNews) {
        List<Repository> repositoryOld = viewRepositories.getRepositories();

        if (mFirstLoad && repositoriesNews.size() > 0) {

            viewRepositories.initRecyclerView(repositoriesNews);
            mFirstLoad = false;
        } else if (repositoryOld.size() > 0 || repositoriesNews.size() > 0) {
            repositoryOld.addAll(repositoriesNews);
            viewRepositories.updateRecyclerView(repositoryOld);
        } else {
            viewRepositories.showError("Nenhum repositório foi encontrado");
        }
    }

    @Override
    public void showErroDisplay(String msgErro) {
        viewRepositories.showError(msgErro);
    }

    @Override
    public void showErroDisplay(String msgErro, boolean toDecreasePage) {
        viewRepositories.toDecreasePage();
        viewRepositories.showError(msgErro);
    }


}
