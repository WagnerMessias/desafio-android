package br.eti.wagnermessias.javapop.pullrequests;

import android.support.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import br.eti.wagnermessias.javapop.R;
import br.eti.wagnermessias.javapop.helpers.NetworkHelper;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class PullRequestsActivityTest {

    @Rule
    public ActivityTestRule<PullRequestsActivity> mActivityTestRule = new ActivityTestRule<PullRequestsActivity>(PullRequestsActivity.class);

    private PullRequestsActivity mActivity = null;

    @Before
    public void setUp() throws Exception {
        mActivity = mActivityTestRule.getActivity();
    }

    @Test
    public void testRecyclerView(){
        assertNotNull( mActivity.findViewById(R.id.rv_pullrequests));
    }

    @After
    public void tearDown() throws Exception {
        mActivity = null;
    }

}