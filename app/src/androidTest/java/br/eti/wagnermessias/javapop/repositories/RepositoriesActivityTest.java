package br.eti.wagnermessias.javapop.repositories;

import android.support.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import br.eti.wagnermessias.javapop.R;
import br.eti.wagnermessias.javapop.helpers.NetworkHelper;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class RepositoriesActivityTest {

    @Rule
    public ActivityTestRule<RepositoriesActivity> mActivityTestRule = new ActivityTestRule<RepositoriesActivity>(RepositoriesActivity.class);

    private RepositoriesActivity mActivity = null;

    @Before
    public void setUp() throws Exception {
        mActivity = mActivityTestRule.getActivity();
    }

    @Test
    public void testRecyclerView(){
        assertNotNull( mActivity.findViewById(R.id.rv_repositories));
    }

    @Test
    public void testBtnTryAgain(){
        if(!NetworkHelper.verificaConexao(mActivity.getContexto())) {
            assertNotNull(mActivity.findViewById(R.id.btn_tray_again));
            onView(withId(R.id.btn_tray_again)).perform(click());
        }else{
            assertTrue(true);
        }
    }

    @Test
    public void testGetRepositories(){

        if(NetworkHelper.verificaConexao(mActivity.getContexto())) {
            assertTrue(mActivity.getRepositories().size() > 0);
        }else{
            assertTrue(true);
        }
    }

    @After
    public void tearDown() throws Exception {
        mActivity = null;
    }

}